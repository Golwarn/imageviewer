﻿/*
 * Copyright 2019 Thomas Krause
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Golwarn.Imager
{
    public partial class MainWindow : Window, IDisposable
    {
        private const string DEFAULT_TITLE = "Image Viewer";
        private const double ZOOM_IN = 1.2;
        private const double ZOOM_OUT = 1.0 / ZOOM_IN;

        private DirectoryInfo directory;
        private IEnumerable<FileInfo> files;
        private FileInfo currentFile;
        private FileSystemWatcher watcher;
        private int index;

        private Point scrollMousePoint = new Point();
        private double horizontalOff = 1;
        private double verticalOff = 1;

        public MainWindow()
        {
            InitializeComponent();
            files = new List<FileInfo>();
            InitFileSystemWatcher();
            menu.Visibility = Visibility.Visible;
            index = 0;
            Title = DEFAULT_TITLE;
        }

        public MainWindow(string path)
            : this()
        {
            menu.Visibility = Visibility.Collapsed;
            LoadPath(path);
        }

        #region FileSystemWatcher

        private void InitFileSystemWatcher()
        {
            if (watcher != null)
                watcher.Dispose();
            watcher = new FileSystemWatcher();
            watcher.Changed += Watcher_Changed;
            watcher.Created += Watcher_Changed;
            watcher.Deleted += Watcher_Changed;
            watcher.Renamed += Watcher_Changed;
            watcher.Error += Watcher_Error;
        }

        private void Watcher_Error(object sender, ErrorEventArgs e)
        {
            Watcher_Changed(sender, null);
            if (directory.Exists)
                InitFileSystemWatcher();
            watcher.Path = directory.FullName;
        }

        private void Watcher_Changed(object sender, FileSystemEventArgs e)
        {
            if (directory.Exists)
                files = directory.GetFiles().ToList();
            else
                files = new List<FileInfo> { currentFile };
        }

        #endregion FileSystemWatcher

        private void LoadPath(string path)
        {
            directory = new FileInfo(path).Directory;
            files = directory.EnumerateFiles().Where(x => x.Name.EndsWith(".JPG", true, CultureInfo.CurrentCulture) || x.Name.EndsWith(".JPEG", true, CultureInfo.CurrentCulture) || x.Name.EndsWith(".PNG", true, CultureInfo.CurrentCulture));
            FileInfo file = null;
            for (int i = 0; i < files.Count(); i++)
            {
                file = files.Skip(i).First();
                if (file.FullName.Equals(path))
                {
                    index = i;
                    break;
                }
            }
            ShowPicture(file);
            watcher.Path = directory.FullName;
            watcher.EnableRaisingEvents = true;
        }

        private void ShowPicture(FileInfo file)
        {
            BitmapImage image = new BitmapImage(new Uri(file.FullName));
            Console.WriteLine(image.Height);
            Console.WriteLine(image.PixelHeight);
            Console.WriteLine(image.DpiX);
            image1.Source = image;
            scrollViewer.Focus();
            currentFile = file;
            Title = DEFAULT_TITLE + " - " + currentFile.FullName;
        }

        #region menu

        private void MenuOpen_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                menu.Visibility = Visibility.Collapsed;
                LoadPath(op.FileName);
            }
        }

        private void MenuClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void MenuReset_Click(object sender, RoutedEventArgs e)
        {
            SetSize(0.0);
        }

        #endregion menu

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Right)
            {
                if (scrollViewer.ComputedVerticalScrollBarVisibility != Visibility.Visible && scrollViewer.ComputedHorizontalScrollBarVisibility != Visibility.Visible && files.Count() > 1)
                {
                    index = ++index % files.Count();
                    ShowPicture(files.Skip(index).First());
                }
            }
            else if (e.Key == Key.Left)
            {
                if (scrollViewer.ComputedVerticalScrollBarVisibility != Visibility.Visible && scrollViewer.ComputedHorizontalScrollBarVisibility != Visibility.Visible && files.Count() > 1)
                {
                    index--;
                    if (index < 0)
                        index = files.Count() - 1;
                    ShowPicture(files.Skip(index).First());
                }
            }
            else if (e.Key == Key.LeftAlt || e.Key == Key.RightAlt || e.Key == Key.System)
                menu.Visibility = menu.Visibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
            else if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                if (e.Key == Key.OemPlus || e.Key == Key.Add)
                    Zoom(ZoomDirection.In);
                else if (e.Key == Key.OemMinus || e.Key == Key.Subtract)
                    Zoom(ZoomDirection.Out);
            }
            else if (e.Key == Key.Escape)
                SetSize(0.0);
            e.Handled = true;
        }

        public void Dispose()
        {
            if (this.watcher != null)
                watcher.Dispose();
        }

        private void Image1_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (!Keyboard.IsKeyDown(Key.LeftAlt) && !Keyboard.IsKeyDown(Key.RightAlt) && !Keyboard.IsKeyDown(Key.System) && !Keyboard.IsKeyDown(Key.LeftCtrl) && !Keyboard.IsKeyDown(Key.RightCtrl))
            {
                Zoom(e.Delta > 0 ? ZoomDirection.In : ZoomDirection.Out);
                e.Handled = true;
            }
            else if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                scrollViewer.ScrollToHorizontalOffset(scrollViewer.HorizontalOffset + e.Delta);
                e.Handled = true;
            }
        }

        private void Zoom(ZoomDirection direction)
        {
            Console.WriteLine("Zoom");
            double delta = direction == ZoomDirection.In ? ZOOM_IN : ZOOM_OUT;
            Matrix matrix = image1.LayoutTransform.Value;
            matrix.ScaleAt(delta, delta, Mouse.GetPosition(this).X, Mouse.GetPosition(this).Y);
            image1.LayoutTransform = new MatrixTransform(matrix);
        }

        private enum ZoomDirection
        {
            In, Out
        }

        /// <summary>
        /// If zoomed, move image instead of next image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScrollViewer_KeyUp(object sender, KeyEventArgs e)
        {
            Window_KeyUp(sender, e);
        }

        #region mouse drag

        private void ScrollViewer_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            scrollMousePoint = e.GetPosition(scrollViewer);
            horizontalOff = scrollViewer.HorizontalOffset;
            verticalOff = scrollViewer.VerticalOffset;
            scrollViewer.CaptureMouse();
        }

        private void ScrollViewer_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (scrollViewer.IsMouseCaptured)
            {
                scrollViewer.ScrollToHorizontalOffset(horizontalOff + (scrollMousePoint.X - e.GetPosition(scrollViewer).X));
                scrollViewer.ScrollToVerticalOffset(verticalOff + (scrollMousePoint.Y - e.GetPosition(scrollViewer).Y));
            }
        }

        private void ScrollViewer_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            scrollViewer.ReleaseMouseCapture();
        }

        #endregion mouse drag

        private void SetSize(double value)
        {
            image1.LayoutTransform = new TranslateTransform(value, value);
        }

    }
}
